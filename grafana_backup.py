#!/usr/bin/env python3
""" Backup grafana json """

import configparser
import json
import os
import requests
import sys


class Grafana:
    '''get/list dashboards, folders and datasources'''

    # pylint: disable=too-many-instance-attributes
    # I need all the attributes! LOL

    def __init__(self, config):
        ''' Init values '''
        self.url = config.get("Grafana", "URL")
        self.token = config.get("Grafana", "TOKEN")
        self.host = config.get("Grafana", "HOST")
        self.verify_ssl = config.get("Grafana", "VERIFY_SSL")
        self.search_api_limit = config.get("Grafana", "SEARCH_API_LIMIT")
        self.http_get_headers = {'Authorization': 'Bearer ' + self.token,
                                 'Host': self.host}

    def health_check(self):
        ''' Health check grafana url '''
        url = self.url + '/api/health'
        print('grafana health: {0}'.format(url))
        return self.send_http_get(url)

    def get_folder_list(self):
        ''' get a list of folders '''
        print('list of folders:')
        return self.send_http_get(self.url + '/api/search/?type=dash-folder')

    def get_folder(self, uid):
        ''' get a folder content '''
        (status, content) = self.send_http_get(
            self.url + '/api/folders/{0}'.format(uid))
        print('query folder:{0}, status:{1}'.format(uid, status))
        return (status, content)

    def get_dashboard_list(self, page):
        ''' get a list of dashboards '''
        page_url = self.url + \
            '/api/search/?type=dash-db&limit={0}&page={1}'.format(
                self.search_api_limit, page)
        print('List of dashboard in grafana: {0}'.format(page_url))
        return self.send_http_get(page_url)

    def get_dashboard(self, uri):
        ''' get a dashboard content '''
        board_url = self.url + '/api/dashboards/{0}'.format(uri)
        print('query dashboard uri: {0}'.format(board_url))
        (status, content) = self.send_http_get(board_url)
        return (status, content)

    def get_datasource_list(self):
        ''' get a list of datasources '''
        print('List of datasources:')
        return self.send_http_get(self.url + '/api/datasources')

    def send_http_get(self, url):
        ''' general http get request '''
        req = requests.get(url, headers=self.http_get_headers)
        return (req.status_code, req.json())


def get_list_of_folders(grafana):
    ''' get a list of folder '''
    (status, content) = grafana.get_folder_list()
    folders = content
    if status == 200:
        print('There are {0} folders:'.format(len(content)))
        for folder in folders:
            print('Name {0}'.format(folder['title']))
    else:
        print('get folders failed, status: {0}, msg: {1}'.format(
            status, content))
    return folders


def get_folders(grafana, output_path, folders):
    ''' get all folders with content '''
    for folder in folders:
        (status, content) = grafana.get_folder(folder['uid'])
        if status == 200:
            save_folder_json(output_path,
                             folder['title'], folder['uid'], content)


def save_folder_json(output_path, folder_name, file_name, folder_json):
    ''' save folder json to file '''
    file_path = output_path + '/folders/' + file_name + '.folder'
    with open(file_path, 'w') as output:
        output.write(json.dumps(folder_json))
    print('folder:{0} are saved to {1}'.format(folder_name, file_path))


def get_list_of_dashboards(grafana, page):
    ''' get a list of dashboards '''
    (status, content) = grafana.get_dashboard_list(page)
    dashboards = content
    if status == 200:
        print('There are {0} dashboards:'.format(len(dashboards)))
        for board in dashboards:
            print('name: {}'.format(board['title']))
    else:
        print('Failed to get dashboards, status: {0}, msg: {1}'.format(
            status, content))
    return dashboards


def get_dashboards(grafana, output_path, dashboards):
    ''' get all dashboards with contents '''
    for dashboard in dashboards:
        (status, content) = grafana.get_dashboard(dashboard['uri'])
        if status == 200:
            save_dashboard_json(output_path,
                                dashboard['title'], dashboard['uid'], content)


def save_dashboard_json(output_path, dashboard_name, file_name,
                        dashboard_json):
    ''' save dashboard content to json '''
    file_path = output_path + '/dashboards/' + file_name + '.dashboard'
    with open(u"{0}".format(file_path), 'w') as output:
        output.write(json.dumps(dashboard_json))
    print('dashboard: {0} saved to: {1}'.format(dashboard_name, file_path))


def save_datasource_json(output_path, file_name, datasource_json):
    ''' save datasource content to json '''
    file_path = output_path + '/datasources/' + file_name + '.datasource'
    with open(file_path, 'w') as output:
        output.write(json.dumps(datasource_json))
        print('datasource:{0} is saved to {1}'.format(file_name, file_path))


def backup_folders(grafana, output_path):
    ''' backup folder wrapper '''
    folders = get_list_of_folders(grafana)
    get_folders(grafana, output_path, folders)


def backup_dashboards(grafana, output_path):
    ''' backup dashboard wrapper '''
    current_page = 1
    while True:
        dashboards = get_list_of_dashboards(grafana, current_page)
        if len(dashboards) == 0:
            break
        current_page += 1
        get_dashboards(grafana, output_path, dashboards)


def backup_datasources(grafana, output_path):
    ''' backup datasource wrapper '''
    (status, content) = grafana.get_datasource_list()
    if status == 200:
        datasources = content
        print('There are {0} datasources:'.format(len(datasources)))
        for datasource in datasources:
            save_datasource_json(output_path, datasource['name'], datasource)
    else:
        print('query datasource failed, status: {}, msg: {}'.format(
            status, content))


def run_backup(grafana, output_path):
    """ Backup EVERYTHING """
    if not os.path.exists(output_path):
        os.makedirs(output_path)
        os.makedirs(output_path + '/folders')
        os.makedirs(output_path + '/dashboards')
        os.makedirs(output_path + '/datasources')
    backup_folders(grafana, output_path)
    backup_dashboards(grafana, output_path)
    backup_datasources(grafana, output_path)


def main():
    """Take the settings for backup"""
    param_config = configparser.ConfigParser()
    param_config.read(os.path.join(sys.path[0], "./settings.conf"))
    output_path = param_config.get("OS", "OUTPUT")
    grafana = Grafana(param_config)
    (status, resp) = grafana.health_check()
    if status == 200:
        run_backup(grafana, output_path)
    else:
        print('Grafana status is not OK: {0}'.format(resp))


if __name__ == '__main__':
    main()
