#!/usr/bin/env python3
""" Restore grafana json """

import configparser
import json
import os
import re
import sys
import requests


class Grafana:
    '''create folders dashboards and datasources'''

    # pylint: disable=too-many-instance-attributes

    def __init__(self, config):
        ''' Init values '''
        self.url = config.get("Grafana", "URL")
        self.token = config.get("Grafana", "TOKEN")
        self.host = config.get("Grafana", "HOST")
        self.verify_ssl = config.get("Grafana", "VERIFY_SSL")
        self.search_api_limit = config.get("Grafana", "SEARCH_API_LIMIT")
        self.http_get_headers = {'Authorization': 'Bearer ' + self.token,
                                 'Host': self.host}
        self.http_post_headers = {'Authorization': 'Bearer ' + self.token,
                                  'Host': self.host,
                                  'Content-Type':  'application/json'}

    def health_check(self):
        ''' Health check grafana url '''
        url = self.url + '/api/health'
        print('grafana health: {0}'.format(url))
        return self.send_http_get(url)

    def create_folder(self, payload):
        ''' create a folder '''
        return self.send_http_post(self.url + '/api/folders', payload)

    def create_datasource(self, payload):
        ''' create a datasource '''
        return self.send_http_post(self.url + '/api/datasources', payload)

    def create_dashboard(self, payload):
        ''' create a dashboard '''
        return self.send_http_post(self.url + '/api/dashboards/db', payload)

    def get_folder(self, uid):
        ''' get a folder content '''
        (status, content) = self.send_http_get(
            self.url + '/api/folders/{0}'.format(uid))
        print('query folder:{0}, status:{1}'.format(uid, status))
        return (status, content)

    def get_folderid(self, folderurl):
        ''' get folderid '''
        if folderurl == "":
            search = re.search(r'dashboards\/[A-Za-z0-9]{1}\/(.*)\/.*',
                               folderurl)
            uid = search.group(1)
            response = self.get_folder(uid)
            if isinstance(response[1], dict):
                folder_data = response[1]
            else:
                folder_data = json.loads(response[1])
            return folder_data['id']
        return 0

    def send_http_get(self, url):
        ''' general http get request '''
        req = requests.get(url, headers=self.http_get_headers)
        return (req.status_code, req.json())

    def send_http_post(self, url, payload):
        ''' general http get request '''
        req = requests.post(url, headers=self.http_post_headers, data=payload,
                            verify=self.verify_ssl)
        return (req.status_code, req.json())


def restore_folders(grafana, input_path):
    ''' restore all folders '''
    for backup_file in os.listdir(input_path + '/folders/'):
        if backup_file.endswith('.folder'):
            backup_file_path = os.path.join(input_path, 'folders',
                                            backup_file)
            with open(backup_file_path, 'r') as payload:
                data = payload.read()
            folder = json.loads(data)
            result = grafana.create_folder(json.dumps(folder))
            print("create folder status: {0}, msg: {1}".format(
                result[0], result[1]))


def restore_dashboards(grafana, input_path):
    ''' restore all dashboards '''
    for backup_file in os.listdir(input_path + '/dashboards/'):
        if backup_file.endswith('.dashboard'):
            backup_file_path = os.path.join(input_path, 'dashboards',
                                            backup_file)
            with open(backup_file_path, 'r') as payload:
                data = payload.read()
            dashboard = json.loads(data)
            # Reset dashboard id
            dashboard['dashboard']['id'] = None
            jsonpayload = {
                'dashboard': dashboard['dashboard'],
                'folderId': grafana.get_folderid(
                    dashboard['meta']['folderUrl']),
                'overwrite': True
            }
            result = grafana.create_dashboard(json.dumps(jsonpayload))
            print("create response status: {0}, msg: {1}".format(result[0],
                                                                 result[1]))


def restore_datasources(grafana, input_path):
    ''' restore all datasources '''
    for backup_file in os.listdir(input_path + '/datasources/'):
        if backup_file.endswith('.datasource'):
            backup_file_path = os.path.join(input_path, 'datasources',
                                            backup_file)
            with open(backup_file_path, 'r') as payload:
                data = payload.read()
            datasource = json.loads(data)
            result = grafana.create_datasource(json.dumps(datasource))
            print("create datasource: {0}, status: {1}, msg: {2}".format(
                datasource['name'], result[0], result[1]))


def run_restore(grafana, input_path):
    """ Restore everything """
    restore_folders(grafana, input_path)
    restore_datasources(grafana, input_path)
    restore_dashboards(grafana, input_path)


def main():
    """Take the settings for restore"""
    param_config = configparser.ConfigParser()
    param_config.read(os.path.join(sys.path[0], "./settings_restore.conf"))
    input_path = param_config.get("OS", "OUTPUT")
    grafana = Grafana(param_config)
    (status, resp) = grafana.health_check()
    if status == 200:
        run_restore(grafana, input_path)
    else:
        print('Grafana status is not OK: {0}'.format(resp))


if __name__ == '__main__':
    main()
