Grafana backup tool
backup folders, dashboard and datasources from grafana as json
This repo is called by rundeck job:
1. git clone over to backup server
2. run grafana_backup.py to dump grafana json under /data/backup which will be copy offsite to google storage

Under backup directory:
1. Sub folders of folders, dashboards and datasources
2. jsons of folders are <folderid>.folder
3. jsons of dashboards are <boardid>.dashboard
4. jsons of datasources are <datasourceid.>.datasource

To Restore:
1. edit settings_restore.conf to restore to a test environment. or
2. copy settings.conf (backup config) into settings_restore.conf if restore to production

